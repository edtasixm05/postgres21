# postgres:compose
## @edt ASIX-M05 Curs 2021-2022

## Crear un acontainer postgres amb Docker-Compose

```
$ docker-compose up -d
Creating network "postgrescompose_default" with the default driver
Pulling adminer (adminer:)...

$ docker ps
CONTAINER ID   IMAGE      COMMAND                  CREATED          STATUS          PORTS                                       NAMES
142c474ee118   postgres   "docker-entrypoint.s…"   31 seconds ago   Up 30 seconds   5432/tcp                                    postgrescompose_db_1
1e589128e71f   adminer    "entrypoint.sh docke…"   31 seconds ago   Up 30 seconds   0.0.0.0:8080->8080/tcp, :::8080->8080/tcp   postgrescompose_adminer_1

$ nmap 172.20.0.2
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-08 18:15 CEST
Nmap scan report for 172.20.0.2
Host is up (0.00011s latency).
Not shown: 999 closed ports
PORT     STATE SERVICE
5432/tcp open  postgresql

$ nmap 172.20.0.3
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-08 18:15 CEST
Nmap scan report for 172.20.0.3
Host is up (0.00011s latency).
Not shown: 999 closed ports
PORT     STATE SERVICE
8080/tcp open  http-proxy

```



