# postgres:base
## @edt ASIX-M05 Curs 2021-2022

## Descripció

Engegar un container basat en una imatge postgres oficial. En aquest cas no cal crear cap imatge nostra, 
simplement utilitzar les imatges prefabricades de psotgres.

Podeu condultar la documentació de:

 * [DockerHub Postgres](https://hub.docker.com/_/postgres)
 * [GitHub Postgres](https://github.com/docker-library/postgres)

Per engegar un simple container amb una base de dades postgres podem fer:
```
docker run --name postgres.edt.org -h postgres.edt.org  -e POSTGRES_PASSWORD=secret -d postgres

$ docker ps
CONTAINER ID   IMAGE      COMMAND                  CREATED          STATUS          PORTS      NAMES
59d006742d6a   postgres   "docker-entrypoint.s…"   26 seconds ago   Up 21 seconds   5432/tcp   postgres.edt.org
```

Des d'una altra sessió podem verificar el funcionament del servidor fent per exemeples un *docker exec* i executant una
consulta SQL
```
$ docker exec -it postgres.edt.org /bin/bash
root@postgres:/# psql -U postgres -C "select 1;"
/usr/lib/postgresql/14/bin/psql: invalid option -- 'C'
Try "psql --help" for more information.
root@postgres:/# psql -U postgres -c "select 1;"
 ?column? 
----------
        1
(1 row)

$ docker exec -it postgres.edt.org psql -U postgres -c "select 1;"
 ?column? 
----------
        1
(1 row)
```

O directament fent consultes des de l'exterior (cal disposar del client psql)
```
 nmap 172.17.0.2
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-08 17:36 CEST
Nmap scan report for ldap.edt.org (172.17.0.2)
Host is up (0.00013s latency).
Not shown: 999 closed ports
PORT     STATE SERVICE
5432/tcp open  postgresql

$ psql -U postgres -c "select 1;"
```

#### Exemples de comandes per engegar el container

```
docker run --name some-postgres -e POSTGRES_PASSWORD=mysecretpassword -d postgres
```

```
docker run --name postgresql -e POSTGRES_USER=myusername -e POSTGRES_PASSWORD=mypassword -p 5432:5432 -v /data:/var/lib/postgresql/data -d postgres
```

```
docker run --name my-pgadmin -p 82:80 -e 'PGADMIN_DEFAULT_EMAIL=user@domain.local' -e 'PGADMIN_DEFAULT_PASSWORD=postgresmaster'-d dpage/pgadmin4
```

```
docker run -d 
    --name postgres 
    -p 5432:5432
    -e POSTGRES_PASSWORD=<password> 
    -v postgres:/var/lib/postgresql/data 
    postgres:14
```

```
docker run -d 
    --name postgres 
    -p 5432:5432
    -e POSTGRES_PASSWORD_FILE=/run/secrets/postgres-password 
    -v ./postgres-password.txt:/run/secrets/postgres-password
    -v postgres:/var/lib/postgresql/data 
    postgres:14
```

```
docker run -d 
    --name postgres 
    -p 5432:5432
    -e POSTGRES_PASSWORD=<password> 
    -v postgres:/var/lib/postgresql/data 
    postgres:14 -c max_connections=100
```

```
docker run -d 
    --name postgres 
    -p 5432:5432
    -e POSTGRES_PASSWORD=<password> 
    -v ./postgres.conf:/etc/postgresql/postgresql.conf 
    -v postgres:/var/lib/postgresql/data 
    postgres:14 -c config_file=/etc/postgresql/postgresql.conf
```


## Crear interactivament la BD training

**pendent acabar**

Aquest exemple és *del tot inapropiat*. Intenta generar la base de dades interactivament
imitant el procés usant a classe per generar la base de dades tarining. primerament cal copiar dins
el container (manualment amb *docker cp*) tots els fitxers de training a /tmp. Llavors executar els 
scripts de creació de taules i els de càrrega de dades.

Tota aquesta tasca no té sentit per:

 * 1) Com que no s'està utilitzant persistència de dades (*volumes*) les dades es perdran en tancar el container.
 * 2) Aquest és un mecanisme pèsssim de generar la base de dades. veurem que la propia imatge *postgres* proporciona
      un mecanisme autotamitzat basat en el directori */docker-entrypoint-initdb.d*.

```
[ecanet@mylaptop postgres:oficial]$ docker exec -it postgres.edt.org psql -U postgres -c "CREATE USER edtasixm05;"
CREATE ROLE
[ecanet@mylaptop postgres:oficial]$ docker exec -it postgres.edt.org psql -U postgres -c "create database training;"
CREATE DATABASE
[ecanet@mylaptop postgres:oficial]$ docker exec -it postgres.edt.org psql -U postgres -c "grant ALL ON DATABASE training to edtasixm05;"
GRANT
```

```
 docker exec -it postgres.edt.org psql -U postgres training
psql (14.2 (Debian 14.2-1.pgdg110+1))
Type "help" for help.

training=# \i /tmp/oficinas.sql
CREATE TABLE
training=# \i /tmp/poductos.sql
/tmp/poductos.sql: No such file or directory

training=# \copy oficinas from  /tmp/oficinas.dat
COPY 5
training=# \copy productos from productos.dat
productos.dat: No such file or directory

$ docker exec -it postgres.edt.org psql -U postgres training -c "select * from oficinas;"
 oficina |   ciudad    | region | dir | objetivo  |  ventas   
---------+-------------+--------+-----+-----------+-----------
      22 | Denver      | Oeste  | 108 | 300000.00 | 186042.00
      11 | New York    | Este   | 106 | 575000.00 | 692637.00
      12 | Chicago     | Este   | 104 | 800000.00 | 735042.00
      13 | Atlanta     | Este   | 105 | 350000.00 | 367911.00
      21 | Los Angeles | Oeste  | 108 | 725000.00 | 835915.00
(5 rows)
```

## Executar un container amb consola PSQL directament

Podem engegar un container oficial postgres i generar directament una consola interactiva de *psql* 
pasant aquesta instrucció com a *entrypoint*.

**pendent refer!!**

```
$ docker run -it --rm  -h postgres.edt.org postgres --net mynet -e POSTGRES_PASSWORD=secret POSTGRES_HOST_AUTH_METHOD=trust  psql -h postgres.edt.org  -U postgres

```





