#! /bin/bash

psql template1
CREATE USER edtasixm06;
#grant ALL ON DATABASE template1 TO edtasixm06;
create database training;
grant ALL ON DATABASE training to edtasixm06;
#grant ALL on oficinas, pedidos, productos, repventas, clientes to edtasixm06;

\q
psql training
\i /opt/docker/training/pedidos.sql
\i /opt/docker/training/productos.sql
\i /opt/docker/training/clientes.sql
\i /opt/docker/training/repventas.sql

grant ALL on oficinas, pedidos, productos, repventas, clientes to edtasixm06;
\copy pedidos from /opt/docker/training/pedidos.dat 
\copy productos from /opt/docker/training/productos.dat 
\copy clientes from /opt/docker/training/clientes.dat  
\copy repventas from /opt/docker/training/repventas.dat 
\copy oficinas from /opt/docker/training/oficinas.dat  
