# postgres:custom
## @edt ASIX-M05 Curs 2021-2022


```
docker run -d 
    --name postgres 
    -p 5432:5432
    -e POSTGRES_PASSWORD=<password> 
    -v ./db-seed-files/:/etc/docker-entrypoint-initdb.d 
    -v postgres:/var/lib/postgresql/data 
    postgres:14
```

```
FROM postgres:14
COPY postgres.conf /etc/postgresql/postgresql.conf
COPY db-seed-files/ /etc/docker-entrypoint-initdb.d/
CMD ["-c", "config_file=/etc/postgresql/postgresql.conf"]
```

```
docker build -t custom-postgres:latest .
```

```
docker run -d 
    --name custom-postgres 
    -p 5432:5432
    -e POSTGRES_PASSWORD=<password> 
    -v postgres:/var/lib/postgresql/data 
    custom-postgres:latest
```

--- 

[documentació](https://stackoverflow.com/questions/26598738/how-to-create-user-database-in-script-for-docker-postgres)

**Mètode:1**
init.sql
```
CREATE USER docker;
CREATE DATABASE docker;
GRANT ALL PRIVILEGES ON DATABASE docker TO docker;
```

Dockerfile
```
FROM library/postgres
COPY init.sql /docker-entrypoint-initdb.d/
```
```
docker run --rm --name postgres.edt.org -h postgres.edt.org -e  POSTGRES_PASSWORD=secret -d edtasixm05/postgres:custom
```


**Mètode2:**

Crea l'usuari, assigna el passwd i crea la base de dades
```
docker run -e POSTGRES_USER=docker -e POSTGRES_PASSWORD=docker -e POSTGRES_DB=docker library/postgres
```
FROM library/postgres
ENV POSTGRES_USER docker
ENV POSTGRES_PASSWORD docker
ENV POSTGRES_DB docker
```







