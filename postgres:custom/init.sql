CREATE USER docker;
CREATE DATABASE training;
GRANT ALL PRIVILEGES ON DATABASE training TO docker;

CREATE TABLE "oficinas" (
        "oficina" int2 NOT NULL,
        "ciudad" character varying(15) NOT NULL,
        "region" character varying(10) NOT NULL,
        "dir" int2,
        "objetivo" numeric(9,2),
        "ventas" numeric(9,2) NOT NULL,
	 primary key (oficina)
);

CREATE TABLE "repventas" (
        "num_empl" int2 NOT NULL,
        "nombre" character varying(15) NOT NULL,
        "edad" int2,
        "oficina_rep" int2,
        "titulo" character varying(10),
        "contrato" date NOT NULL,
        "director" int2,
        "cuota" numeric(8,2),
        "ventas" numeric(8,2) NOT NULL,
	primary key (num_empl)
);


