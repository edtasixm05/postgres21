#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
	#CREATE USER docker;
	#CREATE DATABASE docker;
	#GRANT ALL PRIVILEGES ON DATABASE docker TO docker;

	CREATE USER edtasixm05;
	create database training;
	grant ALL ON DATABASE training to edtasixm05;
	#grant ALL on oficinas, pedidos, productos, repventas, clientes to edtasixm06;

	\c training
	\i /opt/docker/training/pedidos.sql
	\i /opt/docker/training/productos.sql
	\i /opt/docker/training/clientes.sql
	\i /opt/docker/training/repventas.sql

	\copy pedidos from /opt/docker/training/pedidos.dat
	\copy productos from /opt/docker/training/productos.dat
	\copy clientes from /opt/docker/training/clientes.dat
	\copy repventas from /opt/docker/training/repventas.dat
	\copy oficinas from /opt/docker/training/oficinas.dat
EOSQL
