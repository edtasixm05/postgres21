


#### Exemples del servei postgres

Engegar un servidor en detach:
```
$ docker run --name some-postgres -e POSTGRES_PASSWORD=mysecretpassword --net mynet -d postgres
5d72b3e2f251cb6b25ba2793819e48caa63b1763d2574008a7173b2225754d50

$ docker ps
CONTAINER ID   IMAGE      COMMAND                  CREATED         STATUS         PORTS      NAMES
5d72b3e2f251   postgres   "docker-entrypoint.s…"   6 seconds ago   Up 5 seconds   5432/tcp   some-postgres

$ docker stop some-postgres
```


Engegar un servidor per treballar-hi interacticament amb psql
```
$ docker run  --rm --net mynet -it  postgres psql -h some-postgres  -U postgres
Password for user postgres: 
psql (14.3 (Debian 14.3-1.pgdg110+1))
Type "help" for help.

postgres=# 	
```

Engegar amb un docker-compose
```
docker-compose -f stack.yml up

# Connectar a http://ip:8080
# Use postgres/example user/password credentials
# System: PostgreSQL (atenció!)
# Server: db
# Username: postgres
# Password: example
# Database: postgres
```

Engegar amb docker stack
```
docker stack deploy -c stack.yml postgres

# Connectar a http://ip:8080
# Use postgres/example user/password credentials
# System: PostgreSQL (atenció!)
# Server: db
# Username: postgres
# Password: example
# Database: postgres
```

Autenticació
 * És obligatoria la variable POSTGRES_PASSWORD per identificar el password de l'usuari
   administrador de postgres.
 * Aquest usuari (normalment *postgres*) es pot definir a la variable POATGRES_USER.
 * Per defecte les connexions a *localhost* són trusted i no demana password, les remotes si.

```
$ docker exec -it postgres_db_1 /bin/bash
root@12361ff1713e:/# psql  -U postgres
psql (14.3 (Debian 14.3-1.pgdg110+1))
Type "help" for help.
postgres=# 
```


#### Base de dades training


Crear una BD amb nom propi
```
docker run --name training  -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training -d postgre

$ docker exec -it training  psql -U postgres
psql (14.3 (Debian 14.3-1.pgdg110+1))
Type "help" for help.

postgres=# \c
You are now connected to database "postgres" as user "postgres".
postgres=# \q

$ docker exec -it training  psql -U postgres -d training 
psql (14.3 (Debian 14.3-1.pgdg110+1))
Type "help" for help.

training=# \c
You are now connected to database "training" as user "postgres".

training=# \l
                                 List of databases
   Name    |  Owner   | Encoding |  Collate   |   Ctype    |   Access privileges   
-----------+----------+----------+------------+------------+-----------------------
 postgres  | postgres | UTF8     | en_US.utf8 | en_US.utf8 | 
 template0 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
           |          |          |            |            | postgres=CTc/postgres
 template1 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
           |          |          |            |            | postgres=CTc/postgres
 training  | postgres | UTF8     | en_US.utf8 | en_US.utf8 | 
(4 rows)

training=#\q 
```


Inicialitzar la BD
```
$ docker run --rm --name training  -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training -v $(pwd)/training:/docker-entrypoint-initdb.d -d postgres

$ docker exec -it training  psql -U postgres -d training 
psql (14.3 (Debian 14.3-1.pgdg110+1))
Type "help" for help.

training=# \d
           List of relations
 Schema |   Name    | Type  |  Owner   
--------+-----------+-------+----------
 public | clientes  | table | postgres
 public | oficinas  | table | postgres
 public | pedidos   | table | postgres
 public | productos | table | postgres
 public | repventas | table | postgres
(5 rows)


training=# select * from oficinas;
 oficina |   ciudad    | region | dir | objetivo  |  ventas   
---------+-------------+--------+-----+-----------+-----------
      22 | Denver      | Oeste  | 108 | 300000.00 | 186042.00
      11 | New York    | Este   | 106 | 575000.00 | 692637.00
      12 | Chicago     | Este   | 104 | 800000.00 | 735042.00
      13 | Atlanta     | Este   | 105 | 350000.00 | 367911.00
      21 | Los Angeles | Oeste  | 108 | 725000.00 | 835915.00
(5 rows)
```


#### Exemples utilització training


Generar un container amb la bD training sempre inicialitzada de nou
```
$ docker run --rm --name training  -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training -v $(pwd):/docker-entrypoint-initdb.d -d postgres

$ docker exec -it training  psql -U postgres -d training  
psql (14.3 (Debian 14.3-1.pgdg110+1))
Type "help" for help.

training=# \q
```


Generar un container amb una base de dades training que perdura

<des del directori actiu training>

```
$ docker run --rm --name training  -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training -v $(pwd):/docker-entrypoint-initdb.d  -v training-data:/var/lib/postgresql/data -d postgres
2b8a6a18f0260d67eb7e91b105789db87e710703009a5763dffbc28038e0a1a0

 docker exec -it training  psql -U postgres -d training  
psql (14.3 (Debian 14.3-1.pgdg110+1))
Type "help" for help.

training=# \d
           List of relations
 Schema |   Name    | Type  |  Owner   
--------+-----------+-------+----------
 public | clientes  | table | postgres
 public | oficinas  | table | postgres
 public | pedidos   | table | postgres
 public | productos | table | postgres
 public | repventas | table | postgres
(5 rows)

# drop table oficinas;
DROP TABLE
training=# \q

$ docker stop training 
training

$ docker run --rm --name training  -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training -v $(pwd):/docker-entrypoint-initdb.d  -v training-data:/var/lib/postgresql/data -d postgres
e6855981d54cff843b2315e31b3338ac925705857103a63ed76e0751d0f340b3

 docker exec -it training  psql -U postgres -d training  
psql (14.3 (Debian 14.3-1.pgdg110+1))
Type "help" for help.

training=# \d
           List of relations
 Schema |   Name    | Type  |  Owner   
--------+-----------+-------+----------
 public | clientes  | table | postgres
 public | pedidos   | table | postgres
 public | productos | table | postgres
 public | repventas | table | postgres
(4 rows)

training=# select * from oficinas;
ERROR:  relation "oficinas" does not exist
LINE 1: select * from oficinas;
                      ^
```

### Crear una nova imatge  per a training


Dockerfile
```
FROM library/postgres
ADD . .
COPY training/* /docker-entrypoint-initdb.d/
```

Generar la imatge
```
$ docker build -t edtasixm05/getstarted:postgres .
```


Engegar un container basat en la imatge de training
```
$ docker run --rm --name training  -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training  -d edtasixm05/getstarted:postgres 
b7bad4a14f99a867fa39f238c3a9822eeac6f995c1b63c770b1954db3c4cc5fc

 docker ps
CONTAINER ID   IMAGE                            COMMAND                  CREATED         STATUS         PORTS      NAMES
b7bad4a14f99   edtasixm05/getstarted:postgres   "docker-entrypoint.s…"   4 seconds ago   Up 3 seconds   5432/tcp   training

$ docker exec -it training  psql -U postgres -d training 
psql (14.3 (Debian 14.3-1.pgdg110+1))
Type "help" for help.

training=# \q
```

Des d'un altre host també es pot accedit remotament via psql al postgres a la base de dades
traininf, cal identificar-se amb el password *passwd*.
```
$ docker run -it --rm  postgres psql -h 172.17.0.2 -U postgres -d training
Password for user postgres: 
psql (14.3 (Debian 14.3-1.pgdg110+1))
Type "help" for help.

training=# \d
           List of relations
 Schema |   Name    | Type  |  Owner   
--------+-----------+-------+----------
 public | clientes  | table | postgres
 public | oficinas  | table | postgres
 public | pedidos   | table | postgres
 public | productos | table | postgres
 public | repventas | table | postgres
(5 rows)
training=# \q
```


### Generar um docker-compose, també amb administració gràfica


```
$ docker-compose -f training.yml up -d
Creating network "postgres_default" with the default driver
Creating postgres_db_1      ... done
Creating postgres_adminer_1 ... done


# Connectar a http://ip:8080
# Use postgres/example user/password credentials
# System: PostgreSQL (atenció!)
# Server: db
# Username: postgres
# Password: passwd
# Database: training

$ docker-compose -f training.yml down
Stopping postgres_adminer_1 ... done
Stopping postgres_db_1      ... done
Removing postgres_adminer_1 ... done
Removing postgres_db_1      ... done
Removing network postgres_default
```




