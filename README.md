# postgres
## @edt ASIX-M05 Curs 2021-2022

## Postgres

Aquest repositori acull un conunt de tècniques i imatges per poder desplegar
bases de dades usant postgres.

 * **postgres:oficial** Aquest directori explica com engegar un container amb una imatge 
   de postgres oficial, en blanc. És a dir, s'engega el servidor però no hi ha realment
   base de dades amb xixa.

  * **training** Fitxers per a la creació de la base de dades *training* utilitzada en 
    l'assignatura de base de dades.

  * **postgres:compose** Aquest directori conté el fitxer per a *docker-compose* o *docker stack*
    per a desplegar automàticament el servei postgres i també l'eina gràfica *adminer* que permet
    l'administració gràfica de Postgres. És una eina com *phpMyAdmin* que permet la connexió a 
    tot tipus de base de dades.


 
